-- migrations/20220814191655_add_status_to_subscriptions.sql
-- Add status field to subscriptions table
ALTER     TABLE subscriptions
ADD       COLUMN status text NULL;
