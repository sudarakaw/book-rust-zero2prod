-- migrations/20220814201638_create_subscription_tokens_table.sql
-- Create Subscription Tokens Table
CREATE    TABLE subscription_tokens(
                    subscription_token TEXT NOT NULL
                  , subscriber_id uuid NOT NULL REFERENCES subscriptions(id)
                  , PRIMARY key(subscription_token)
                    );
