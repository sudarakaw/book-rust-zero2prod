-- migrations/20220823064344_rename_password_column.sql
--
ALTER     TABLE users rename password TO password_hash;
