-- migrations/20220905070311_create_newsletter_issues_table.sql
--
CREATE    TABLE newsletter_issues(
                    newsletter_issue_id UUID NOT NULL
                  , title TEXT NOT NULL
                  , text_content TEXT NOT NULL
                  , html_content TEXT NOT NULL
                  , published_at TEXT NOT NULL
                  , PRIMARY key(newsletter_issue_id)
                    );
