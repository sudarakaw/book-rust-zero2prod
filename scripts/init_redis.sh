#!/usr/bin/env sh

set -x

# if a redis container is running, print instructions to kill it and exit
RUNNINT_CONTAINER=$(docker ps --filter 'name=redis' --format '{{.ID}}')
if [ -n "$RUNNINT_CONTAINER" ]; then
  echo >&2 "There is a Redis container already running, kill it with"
  echo >&2 "    docker kill ${RUNNINT_CONTAINER}"

  exit 1
fi

# Launch Redis using Docker
# Allow to skip Docker if a dockerized Postgres database is already running
docker run \
  -p 6379:6379 \
  -d \
  --name "redis_$(date '+%s')" \
  redis

>&2 echo "Redis is ready to go!"
