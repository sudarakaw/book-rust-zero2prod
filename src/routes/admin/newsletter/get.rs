//! src/routes/admin/newsletter/get.rs

use actix_web::{http::header::ContentType, HttpResponse};
use actix_web_flash_messages::IncomingFlashMessages;
use uuid::Uuid;

use std::fmt::Write;

pub async fn newsletter_form(
    flash_messages: IncomingFlashMessages,
) -> Result<HttpResponse, actix_web::Error> {
    let mut msg_html = String::new();
    let idempotency_key = Uuid::new_v4().to_string();

    for m in flash_messages.iter() {
        writeln!(msg_html, "<p><i>{}</i></p>", m.content()).unwrap();
    }

    Ok(HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(format!(
            r#"<!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta http-equiv="content-type" content="text/html; charset=utf-8">
                    <title>Send Newsletter Issue</title>
                </head>
                <body>
                {msg_html}
                <form action="/admin/newsletters" method="post">
                <label>Title
                    <input type="text" placeholder="Enter newsletter isue title" name="title" />
                </label>

                <label>HTML
                    <textarea placeholder="Enter HTML content" name="content_html"></textarea>
                </label>

                <label>Text
                    <textarea placeholder="Enter text content" name="content_text"></textarea>
                </label>

                <input type="hidden" name="idempotency_key" value="{idempotency_key}" />
                <button type="submit">Send newsletter</button>
                </form>
                <p><a href="/admin/dashboard">&lt;- Back</a></p>
                </body>
                </html>
                "#
        )))
}
